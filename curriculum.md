# Curriculum Vitae #

![foto](/images/imatge.png)


## Datos Personales ##

**nombre y apellidos**: Jordi German Quiñe Dongo

**Dirección**: C/cristobal de moura 217

**teléfono**: 642730283

**Correo**: [jordi-qd@hotmail.com](jordi-qd@hotmail.com) , [46986035w@iespoblenou.org](46986035w@iespoblenou.org)

## Formación académica   ##

- **2012-2016 Graduado de educación secundaria obligatoria ESO**
- **2017-2020 Ciclo Formativo grado medio de Técnico en Sistemas
Microinformático y Redes.**

## Informática ##

- **Windows**
- **Linux**
- **Microsoft word, Write**
- **Excel,Calc**
- **Access,Bases**
- **Internet**
- **Power point,impress**

## Idiomas ##

- **Castellano (Español)**
- **Catalán B3 (A bàsic)**

## Datos de interés ##

- **Curso particular de catalán**
- **disponibilidad a todas horas**
- **persona comprometida con el trabajo.**

