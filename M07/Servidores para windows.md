# Programas servidores para windows:

## XAMPP

Es uno de los más conocidos e incorpora un servidor Apache,<p> un sistema gestor de
bases de datos MySQL y lenguajes como PHP y Perl.

![imagen](images/XAMPP_Windows_10.png)

## WAMPServer

Es un pc con windows que dispone de un servidor Apache, un gestor de bases de datos MySQL y el lenguaje de programación PHP.

![imagen](images/Screenshot_1.png)

## NMPserver

Es un microservidor eficiente que en lugar de apache, incorpora el eficiente servidor web Nginx, junto a bases de datos MySQL y soporte PHP.

![imagen](images/Screenshot_2.png)

## PortableWebServer

PWS (Apache + MySQL + PHP) es un servidor web ligero y portable para Windows que destaca por su directa interfaz, donde con pocos clics podemos modificar opciones de configuración o activar o desactivar módulos de Apache o PHP.

![imagen](images/Screenshot_3.png)

## UwAmp 

Es un servidor para Windows con una interfaz muy cómoda y útil. Además de las opciones comunes, ofrece un gráfico de estadísticas de consumo de CPU por servidor,
<p>unos gestores de configuración personalizados para Apache, MySQL y PHP, un administrador de bases de datos SQLite , así como utilidades más frecuentes como PHPMyAdmin o XDebug

![imagen](images/Screenshot_4.png)