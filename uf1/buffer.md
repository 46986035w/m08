## Informe de l'aplicació

#### Què necessito saber abans de fer la presentació

1. **PROPÒSIT** Per a què serveix l'aplicació:

3. **CARACTERÍSTIQUES** Descriu les característiques principals de la aplicació. Minim 8.

<br>

    Característica1: 

    Descriptió1: 

    Imatges1 (Com funciona): 

<br>

    Característica2: 

    Descriptió2: 
    
    Imatges2 (Com funciona): 
    
<br>

Característica3: 

    Descriptión3: 
    
    Imatges3 (Com funciona): 


<br>

    Característica4: 

    Descriptió4: 
    
    Imatges4 (Com funciona): 
    
 <br>

    Característica5: 

    Descriptió5: 

    Imatges5 (Com funciona): 
    
 <br>

    Característica6: 

    Descriptió6: 

    Imatges6 (Com funciona): 
    
 <br>

    Característica7: 

    Descriptió7: 

    Imatges7 (Com funciona): 
    
 <br>

    Característica8: 

    Descriptió8: 

    Imatges8 (Com funciona): 


4. **SITUACIONS** Pensa en 3 situacions diferents on pot ser útil l'aplicació

  - Situació1: 
  
  - Situació2:
  
  - Situació3:
